﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongodbMVC.Models
{
    public class ComputerFilter
    {
        public string ComputerName { get; set; }
        public int? Year { get; set; }
    }
}